import discord
from discord.message import Message
from discord.colour import Colour
from discord.guild import Guild
from discord.channel import TextChannel
from discord.member import Member
import asyncio
from datetime import datetime
from typing import Dict, List, Tuple
from types import FunctionType
from enum import Enum
import pickle
from os import makedirs
from os.path import dirname
import re
from glob import glob

# https://discordapp.com/api/oauth2/authorize?client_id=577754542863810586&permissions=8&scope=bot
# https://docs.google.com/document/d/1w8QyBmyK00mzu9Qzu8YWIOT2Q1Mc6FHo_9SBQSOlAzc/edit?usp=sharing
# TODO: 질문자가 포맷을 정하게 하자
# TODO: answer 시 질문자에게 추가 정보를 요구하는 옵션 만들기
# TODO: 질문/답변/룰 edit 기능 추가. on message edit 있었으니까 그거 쓰면 됨
# TODO: reload 추가

FILE_PATTERN = r'\$FILE_[0123456789]+'


def mention_user(user: int) -> str:
    return "<@" + str(user) + ">"


def mention_channel(channel: int) -> str:
    return "<#" + str(channel) + ">"


def convert_number_to_emoji(num: int) -> str:
    if num == 10:
        return ":keycap_ten:"

    to_return = ""
    while num:
        cipher = num % 10
        num //= 10

        if cipher == 0:
            to_return = ":zero:" + to_return
        elif cipher == 1:
            to_return = ":one:" + to_return
        elif cipher == 2:
            to_return = ":two:" + to_return
        elif cipher == 3:
            to_return = ":three:" + to_return
        elif cipher == 4:
            to_return = ":four:" + to_return
        elif cipher == 5:
            to_return = ":five:" + to_return
        elif cipher == 6:
            to_return = ":six:" + to_return
        elif cipher == 7:
            to_return = ":seven:" + to_return
        elif cipher == 8:
            to_return = ":eight:" + to_return
        elif cipher == 9:
            to_return = ":nine:" + to_return

    return to_return


def log_bot(*texts):
    path = "logs/" + str(datetime.now().date()) + ".txt"
    makedirs(dirname(path), exist_ok=True)
    with open(path, "a+", encoding='utf-8') as log_file:
        log_file.write("[" + str(datetime.now())[:19] + "]: " + " ".join(map(str, texts)) + "\n")


def log(server: Guild, channel: TextChannel, *texts, user: Member=None):
    path = "logs/" + str(server.id) + "/" + str(channel.id) + "/" + str(datetime.now().date()) + ".txt"
    makedirs(dirname(path), exist_ok=True)
    with open(path, 'a+', encoding='utf-8') as log_file:
        to_write = "[" + str(datetime.now())[:19] + "]: "
        to_write += " ".join(map(str, texts))
        if user:
            user_txt = user.display_name + "(" + str(user.id) + ")"
            to_write = to_write.replace("[user]", user_txt)
        log_file.write(to_write + "\n")


def save():
    if 'queue_bot' not in globals():
        return
    for name in queue_bot.attrs_to_save:
        file_name = name + "/" + str(datetime.now())[:19].replace(":", "-") + ".txt"
        makedirs(dirname("data/raw/" + file_name), exist_ok=True)
        # makedirs(dirname("data/text/" + file_name), exist_ok=True)

        data = getattr(queue_bot, name)
        with open("data/raw/" + file_name, 'wb') as attr_file:
            pickle.dump(data, attr_file)
        """
        with open("data/text/" + file_name, 'w', encoding='utf-8') as attr_text_file:
            if name in ["question_queue", "question_being_written"]:
                # value is a list of things that need to be converted to string
                attr_text_file.write("\n\n".join(map(lambda k, v: "[" + str(k) + "]\n" +
                                                                  "\n\n".join((map(str, v))), data.items())))
            elif name in ["notification_setting", "question_being_written", "question_being_answered"]:
                # value needs to be converted to string
                attr_text_file.write("\n\n".join(map(lambda k, v: "[" + str(k) + "]\n" + str(v), data.items())))
            else:
                attr_text_file.write(str(data))
        """
        log_bot("Saved", name)


class ENotification(Enum):
    ALL = "ALL",
    URGENT = "URGENT",
    NEVER = "NEVER"

    def __str__(self) -> str:
        return self._name_


def wrapper(method):
    def wrapped(*args, **kwargs):
        result = method(*args, **kwargs)
        save()
        return result
    return wrapped


class SaveClass(type):
    def __new__(mcs, class_name, bases, class_dict):
        new_class_dict = {}

        # wrap the method of base classes
        for base in bases:
            for name, value in base.__dict__.items():
                if name in ['__setitem__', '__delitem__', 'pop', 'popitem', 'update', 'clear',
                            'append', 'insert', 'extend', 'remove', 'reverse', 'sort']:
                    value = wrapper(value)
                new_class_dict[name] = value

        """
        # wrap the method of subclass
        for name, value in class_dict.items():
            if isinstance(value, FunctionType):
                value = wrapper(value)
            new_class_dict[name] = value
        """

        return type.__new__(mcs, class_name, bases, new_class_dict)


class MyDict(dict, metaclass=SaveClass):
    pass


class MyList(list, metaclass=SaveClass):
    pass


class Question:
    def __init__(self, questioner: int, channel: int, content: str, is_private: bool, is_urgent: bool):
        self.questioner = questioner
        self.channel = channel
        self.content = content
        self.time = str(datetime.now())
        self.is_private = is_private
        self.is_urgent = is_urgent

    def __str__(self):
        to_return = mention_user(self.questioner) + " asked"
        if self.is_urgent and not self.is_private:
            to_return += " an"
        else:
            to_return += " a"
        if self.is_private:
            to_return += " **private**"
        if self.is_urgent:
            to_return += " **urgent**"
        to_return += " question at `" + self.time[:19] + "`"
        to_return += " at " + mention_channel(self.channel)
        to_return += ":\n" + self.content
        return to_return


class QueueBot(discord.Client):
    def __init__(self):
        super().__init__()

        self.command_list = ("help", "login", "logout", "enqueue", "eq", "dequeue", "dq", "queue", "q", "notification",
                             "notif", "answer", "ignore", "rule")

        self.question_taker: Dict[int, int] = MyDict()  # channel : user

        self.question_queue: Dict[int, List[Question]] = MyDict()  # channel : list[question]

        self.notification_setting: Dict[int, ENotification] = MyDict()  # channel : state

        self.question_rule: Dict[int, str] = MyDict()  # channel : str

        self.enqueueing_user: Dict[int, List[int]] = MyDict()  # channel : list[user]
        self.question_being_written: Dict[int, Question] = MyDict()  # user : question

        self.answer_being_written: Dict[int, str] = MyDict()  # channel : str
        self.question_being_answered: Dict[int, Question] = MyDict()  # channel : question
        self.is_answering: Dict[int, bool] = MyDict()  # channel : bool

        self.rule_being_written: Dict[int, str] = MyDict()  # channel : str
        self.is_writing_rule: Dict[int, bool] = MyDict()  # channel : bool

        self.attrs_to_save = ("question_taker", "question_queue", "notification_setting", "question_rule",
                              "enqueueing_user", "answer_being_written", "question_being_answered", "is_answering",
                              "rule_being_written", "is_writing_rule")

        self.load()

        self.logging_in_channel = None
        self.logging_in_user = None
        self.logging_out_channel = None
        self.logging_out_user = None

    async def on_ready(self):
        log_bot("Logged in as", self.user)
        print("Queue Bot Online!"
              "\nPress Ctrl+C to turn the bot down after saying it's offline. (It'll take a while, please wait for it.)"
              "\nTo quit without saying, just hit the X button or anything that kills the process.")

        for active_channel in self.question_taker.keys():
            await self.say(active_channel, (":hugging: Queue Bot Online", "Queue Bot is online now."))

        for server in self.guilds:
            try:
                qb_role = None
                for role in server.get_member(self.user.id).roles:
                    if role.name == "Queue Bot":
                        qb_role = role.id
                        break
                if qb_role:
                    await server.get_role(qb_role).edit(colour=Colour(0x00fcff), position=1)
                    log_bot("Edited the role of server", server.id)
                else:
                    log_bot("Failed to find the Queue Bot role of server", server.id)
            except discord.errors.Forbidden:
                log_bot("Failed to edit the role of server", server.id, "due to the permission error.")

    async def logout(self):
        log_bot("Logged out as", self.user)
        for active_channel in self.question_taker.keys():
            await self.say(active_channel, (":sleeping: Queue Bot Offline", "Queue Bot is offline now."))
        await super().logout()

    async def on_disconnect(self):
        pass

    async def on_message(self, msg: Message):
        if msg.author == self.user:
            return

        if msg.content.startswith(">>"):
            await self.parse_command(msg)
        elif msg.author.id in self.enqueueing_user.get(msg.channel.id, []):
            await self.append_question_content(msg)
        elif self.is_answering.get(msg.channel.id) and msg.author.id == self.question_taker[msg.channel.id]:
            await self.append_answer_content(msg)
        elif self.is_writing_rule.get(msg.channel.id) and msg.author.id == self.question_taker[msg.channel.id]:
            await self.append_rule_content(msg)
        else:
            log(msg.guild, msg.channel, "[user] said:", msg.content, user=msg.author)

    async def parse_command(self, msg: Message):
        arguments = msg.content[2:].split()
        command = arguments.pop(0)
        log(msg.guild, msg.channel, "[user] tried to use command:", msg.content, user=msg.author)
        try:
            command_function = getattr(self, "command_" + command)
        except AttributeError:
            await self.say(msg.channel.id, (":no_entry: Command Failed",
                                            "Unknown Command. Type `>>help` to see all available commands."))
        else:
            try:
                await command_function(msg, *arguments)
            except Exception as e:
                await self.say(msg.channel.id, (":dizzy_face: Oh no!",
                                                "**I'm sorry!**\nAn error had occurred while executing your command."
                                                "\nThe error message has been saved, please contact the developer."))
                log(msg.guild, msg.channel, "!ERROR! An exception had occurred while executing command above.", e)

    async def command_help(self, msg: Message, *args):
        """Show explanation of command(s).
        Additional argument `[command]` can be accepted.
        If `[command]` is given, show the detailed explanation of the `[command]`"""
        if args:
            try:
                await self.say(msg.channel.id, ("`>>" + args[0] + "`", getattr(self, "command_" + args[0]).__doc__))
            except AttributeError:
                await self.say(msg.channel.id, (":no_entry: There's no such command `" + args[0] + "`.",
                                                "Type `>>help` to see all available commands."))
        else:
            await self.say(msg.channel.id, title="All Available Commands", split_line=True,
                           *map(lambda x: ("`>>" + x + "`", getattr(self, "command_" + x).__doc__.split('\n')[0]),
                                self.command_list))

    async def command_login(self, msg: Message, *args):
        """Set this channel's(not server's) Question Receptionist."""
        if msg.channel.id in self.question_taker:
            await self.say(msg.channel.id,
                           (":no_entry: Login Failed", "There is already a question receptionist, " +
                            mention_user(self.question_taker[msg.channel.id])))
            return

        msg_to_send = "Setting " + mention_user(msg.author.id) + " as a question receptionist of this channel."
        msg_to_send += "\nAre you **SURE**?"
        msg_to_send += "\nEnter `Y` to login."
        await self.say(msg.channel.id, (":exclamation: Login Attempt", msg_to_send))

        try:
            self.logging_in_user = msg.author.id
            self.logging_in_channel = msg.channel.id

            response = await self.wait_for("message", check=self.check_login, timeout=5.0)
            if response.content != "Y":
                await self.say(msg.channel.id, (":no_entry: Login Failed", "The user refused."))
                return
            
            self.add_channel(msg.channel.id, msg.author.id)

            msg_to_send = "**"
            msg_to_send += mention_user(self.logging_in_user)
            msg_to_send += " is now a question receptionist of this channel.**"

            await self.say(msg.channel.id, (":white_check_mark: Question Receptionist Set", msg_to_send))

            log(msg.guild, msg.channel, "Set [user] as a question receptionist of this channel.", user=msg.author)
        except asyncio.TimeoutError:
            await self.say(msg.channel.id, (":no_entry: Login Failed", "There was no response."))

    def check_login(self, msg: Message):
        return msg.author.id == self.logging_in_user and msg.channel.id == self.logging_in_channel

    async def command_logout(self, msg: Message, *args):
        """Reset this channel's(not server's) Question Receptionist."""
        if msg.channel.id not in self.question_taker:
            await self.say(msg.channel.id, (":no_entry: Logout Failed",
                                            "There is no question receptionist on this channel yet."
                                            "\nYou have to be the one? Type `>>login`!"))
        elif msg.author.id == self.question_taker[msg.channel.id]:
            msg_to_send = "Resetting the question receptionist of this channel: "
            msg_to_send += mention_user(self.question_taker[msg.channel.id])
            msg_to_send += "\nAre you **SURE**?"
            msg_to_send += "\nEnter `Y` to logout."
            await self.say(msg.channel.id, (":exclamation: Logout Attempt", msg_to_send))

            try:
                self.logging_out_user = msg.author.id
                self.logging_out_channel = msg.channel.id
                response = await self.wait_for("message", check=self.check_logout, timeout=5.0)
                if response.content == "Y":
                    del self.question_taker[msg.channel.id]

                    await self.say(msg.channel.id, (":negative_squared_cross_mark: Question Receptionist Reset",
                                                    "**This channel's question receptionist had been reset.**"))
                    log(msg.guild, msg.channel, "Reset the question receptionist of this channel, [user].",
                        user=msg.author)
                else:
                    await self.say(msg.channel.id, (":no_entry: Logout Failed.", "The user refused."))
            except asyncio.TimeoutError:
                await self.say(msg.channel.id, (":no_entry: Logout Failed.", "There was no response."))
        else:
            await self.say(msg.channel.id, (":no_entry: Logout Failed.",
                                            "You're not the question receptionist of this channel."))

    def check_logout(self, msg: Message):
        return msg.author.id == self.logging_out_user and msg.channel.id == self.logging_out_channel

    async def command_eq(self, msg: Message, *args):
        """Alias for `>>enqueue`"""
        await self.command_enqueue(msg, *args)

    async def command_enqueue(self, msg: Message, *args):
        """Start to enqueue a Question.
        Available options:
            `-private`: make question private, so that your message will be instantly deleted after being stored.
            `-urgent`: make question urgent, it'll take precedence over none-urgent questions. __**USE IT WISELY**__"""
        if msg.author.id in self.enqueueing_user.get(msg.channel.id, []):
            await self.say(msg.channel.id, (":no_entry: Enqueue Failed.", "You're already putting a question."))
            return

        question = Question(msg.author.id, msg.channel.id, "", True if '-private' in args else False,
                            True if '-urgent' in args else False)
        self.question_being_written[msg.author.id] = question

        msg_to_send = "Receiving " + mention_user(msg.author.id) + "'s"
        if question.is_private:
            msg_to_send += " **private**"
        if question.is_urgent:
            msg_to_send += " **urgent**"
        msg_to_send += " Question.\nType `>>end` to finish putting question."
        msg_to_send += "\nType `>>cancel` to cancel enqueueing question."
        msg_to_send += "\n__Other users are **safe** to talk.__"
        if question.is_private:
            msg_to_send += "\n**Your message will be instantly deleted after being stored.**"
        if msg.channel.id in self.notification_setting:
            msg_to_send += "\n\nQuestion Receptionist's notification setting: **" + \
                           str(self.notification_setting[msg.channel.id]) + "**"
            if self.notification_setting[msg.channel.id] == ENotification.ALL:
                msg_to_send += " :bell:"
            elif self.notification_setting[msg.channel.id] == ENotification.URGENT:
                msg_to_send += " :bellhop:"
            else:
                msg_to_send += " :no_bell:"
            msg_to_send += "\nNotification will be sent accordingly."
        if self.question_rule.get(msg.channel.id):
            msg_to_send += "\n\n**:point_up: Here are the rules the question receptionist had set:**\n"
            msg_to_send += self.question_rule[msg.channel.id]
        else:
            msg_to_send += "\n\n**:point_up: There is no rule yet.**\n"
        await self.say(msg.channel.id, (":ear: Receiving a Question", msg_to_send))

        if msg.channel.id not in self.enqueueing_user:
            self.enqueueing_user[msg.channel.id] = MyList()
        self.enqueueing_user[msg.channel.id].append(msg.author.id)

        log(msg.guild, msg.channel, "[user] started to enqueue a question.", user=msg.author)

    async def command_end(self, msg: Message, *args):
        if msg.author.id in self.enqueueing_user.get(msg.channel.id, []):
            if msg.channel.id not in self.question_queue:
                self.question_queue[msg.channel.id] = MyList()

            question_length = len(self.question_queue[msg.channel.id])
            if self.question_being_written[msg.author.id].is_urgent:
                order = 0
                while question_length > order and self.question_queue[msg.channel.id][order].is_urgent:
                    order += 1
            else:
                order = question_length
            self.question_queue[msg.channel.id].insert(order, self.question_being_written[msg.author.id])

            msg_to_send = "Your question was successfully enqueued to order " + convert_number_to_emoji(order + 1)
            await self.say(msg.channel.id, (":ballot_box_with_check: Enqueued a Question", msg_to_send))

            log(msg.guild, msg.channel, "[user] ended enqueueing a question.", user=msg.author)
            log(msg.guild, msg.channel, "Enqueued the following question:\n",
                self.question_being_written[msg.author.id])

            self.enqueueing_user[msg.channel.id].remove(msg.author.id)

            if msg.channel.id not in self.question_taker:
                return

            if self.notification_setting[msg.channel.id] == ENotification.ALL:
                await self.send_dm(msg,
                                   (":bangbang: New Question Enqueued",
                                    "A new question had been enqueued at: " + mention_channel(msg.channel.id) + "."),
                                   user=self.question_taker[msg.channel.id])
            elif self.question_being_written[msg.author.id].is_urgent and \
                    self.notification_setting[msg.channel.id] == ENotification.URGENT:
                await self.send_dm(msg,
                                   (":bangbang: New Question Enqueued", 
                                    "A new **urgent** question had been enqueued at: " +
                                    mention_channel(msg.channel.id) + "."),
                                   user=self.question_taker[msg.channel.id])

            del self.question_being_written[msg.author.id]
        elif msg.author.id == self.question_taker.get(msg.channel.id) and self.is_answering.get(msg.channel.id):
            question = self.question_being_answered[msg.channel.id]

            await self.say(msg.channel.id, (":ballot_box_with_check: Answered a Question",
                                            "Answered " + mention_user(question.questioner) + "'s question."))

            if question.is_private:
                await self.send_dm(msg,
                                   (":slight_smile: **Your Question had been Answered**\n",
                                    self.answer_being_written[msg.channel.id] +
                                    "\n\n**The original question was:**\n" + question.content),
                                   user=question.questioner)
            else:
                await msg.channel.send(mention_user(question.questioner))
                await self.say(msg.channel.id, (":slight_smile: Your Question had been Answered",
                                                self.answer_being_written[msg.channel.id]),
                               ("The original question was:", question.content), split_line=True)

            log(msg.guild, msg.channel, "[user] ended answering a question. The answer is following:\n",
                self.answer_being_written[msg.channel.id], user=msg.author)

            self.answer_being_written[msg.channel.id] = ""
            self.is_answering[msg.channel.id] = False
            del self.question_being_answered[msg.channel.id]
        elif msg.author.id == self.question_taker.get(msg.channel.id) and self.is_writing_rule.get(msg.channel.id):
            self.question_rule[msg.channel.id] = self.rule_being_written[msg.channel.id]

            await self.say(msg.channel.id, (":ballot_box_with_check: Set the Questioning Rule",
                                            "The questioning rule of this channel had been set."))

            self.rule_being_written[msg.channel.id] = ""
            self.is_writing_rule[msg.channel.id] = False

            log(msg.guild, msg.channel, "[user] ended writing the rule. The rules are following:\n",
                self.question_rule[msg.channel.id], user=msg.author)
        else:
            await self.say(msg.channel.id, (":no_entry: Command Failed",
                                            "Unknown command. Type `>>help` to see all available commands."))

    async def command_cancel(self, msg: Message, *args):
        if msg.author.id in self.enqueueing_user.get(msg.channel.id, []):
            del self.question_being_written[msg.author.id]

            await self.say(msg.channel.id,
                           (":no_entry_sign: Enqueueing Cancelled", "Successfully cancelled enqueueing question."))

            self.enqueueing_user[msg.channel.id].remove(msg.author.id)

            log(msg.guild, msg.channel, "[user] cancelled enqueueing a question.", user=msg.author)
        elif msg.author.id == self.question_taker.get(msg.channel.id) and self.is_answering.get(msg.channel.id):
            self.answer_being_written[msg.channel.id] = ""
            self.is_answering[msg.channel.id] = False

            await self.say(msg.channel.id,
                           (":no_entry_sign: Answering Cancelled", "Successfully cancelled answering question."))

            log(msg.guild, msg.channel, "[user] cancelled answering a question.", user=msg.author)
        elif msg.author.id == self.question_taker.get(msg.channel.id) and self.is_writing_rule.get(msg.channel.id):
            self.rule_being_written[msg.channel.id] = ""
            self.is_writing_rule[msg.channel.id] = False

            await self.say(msg.channel.id,
                           (":no_entry_sign: Setting Rules Cancelled", "Successfully cancelled setting rules."))

            log(msg.guild, msg.channel, "[user] cancelled writing rules.", user=msg.author)
        else:
            await self.say(msg.channel.id, (":no_entry: Command Failed",
                                            "Unknown command. Type `>>help` to see all available commands."))

    async def append_question_content(self, msg: Message):
        self.question_being_written[msg.author.id].content += '\n' + msg.content
        for attachment in msg.attachments:
            self.question_being_written[msg.author.id].content += "$FILE_" + str(msg.id)

            path = 'data/attachments/' + str(msg.id) + '.png'
            makedirs(dirname(path), exist_ok=True)
            with open(path, 'wb') as attachment_file:
                await attachment.save(attachment_file, use_cached=True)
                log(msg.guild, msg.channel, "Saved a file while [user]'s writing a question.", user=msg.author)
        log(msg.guild, msg.channel, "Appended to the question [user]'s writing the following:\n", msg.content,
            user=msg.author)
        if self.question_being_written[msg.author.id].is_private:
            await msg.delete()

    async def append_answer_content(self, msg: Message):
        self.answer_being_written[msg.channel.id] += '\n' + msg.content
        for attachment in msg.attachments:
            self.answer_being_written[msg.channel.id] += "$FILE_" + str(msg.id)

            path = 'data/attachments/' + str(msg.id) + '.png'
            makedirs(dirname(path), exist_ok=True)
            with open(path, 'wb') as attachment_file:
                await attachment.save(attachment_file, use_cached=True)
                log(msg.guild, msg.channel, "Saved a file while [user]'s writing an answer.", user=msg.author)
        log(msg.guild, msg.channel, "Appended to the answer [user]'s writing answer the following:\n",
            msg.content, user=msg.author)
        if self.question_being_answered[msg.channel.id].is_private:
            await msg.delete()

    async def append_rule_content(self, msg: Message):
        self.rule_being_written[msg.channel.id] += '\n' + msg.content
        for attachment in msg.attachments:
            self.rule_being_written[msg.channel.id] += "$FILE_" + str(msg.id)

            path = 'data/attachments/' + str(msg.id) + '.png'
            makedirs(dirname(path), exist_ok=True)
            with open(path, 'wb') as attachment_file:
                await attachment.save(attachment_file, use_cached=True)
        log(msg.guild, msg.channel, "Appended to the rule [user]'s writing the following:\n",
            msg.content, user=msg.author)

    async def command_dq(self, msg: Message, *args):
        """Alias for `>>dequeue`"""
        await self.command_dequeue(msg, *args)

    async def command_dequeue(self, msg: Message, *args):
        """Dequeue a Question."""
        if self.question_taker.get(msg.channel.id) != msg.author.id:
            await self.say(msg.channel.id, (":no_entry: Dequeue Failed",
                                            "You're not the question receptionist of this channel."))
            return

        if self.question_being_answered.get(msg.channel.id):
            await self.say(msg.channel.id, (":no_entry: Dequeue Failed",
                                            "You need to answer or ignore the prior question first!"))
            return

        if not self.question_queue.get(msg.channel.id):
            await self.say(msg.channel.id, (":zero: The Queue is Clean!", "There is no question to dequeue."))
            return

        question = self.question_queue[msg.channel.id].pop(0)
        self.question_being_answered[msg.channel.id] = question
        if question.is_private:
            await self.send_dm(msg, (":question: Dequeued Question", str(question)))
        else:
            await self.say(msg.channel.id, (":question: Dequeued Question", str(question)))
        await self.say(msg.channel.id, (":ballot_box_with_check: Dequeued a Question",
                                        "Dequeued " + mention_user(question.questioner) + "'s question."))

        log(msg.guild, msg.channel, "[user] dequeued the following question:\n", question, user=msg.author)

    async def command_q(self, msg: Message, *args):
        """Alias for `>>queue`"""
        await self.command_queue(msg, *args)

    async def command_queue(self, msg: Message, *args):
        """Shows the entire question queue."""
        if not self.question_queue.get(msg.channel.id):
            await self.say(msg.channel.id, (":zero: The Queue is Clean!", 'Nothing to show.'))
            return

        msg_to_send = []
        i = 1
        for question in self.question_queue[msg.channel.id]:
            description = mention_user(question.questioner) + "'s"
            if question.is_private:
                description += " **private**"
            if question.is_urgent:
                description += " **urgent**"
            description += " Question at `" + question.time[:19] + "`"
            msg_to_send.append((convert_number_to_emoji(i), description))
            i += 1

        await self.say(msg.channel.id, *msg_to_send, title=":regional_indicator_q: Question Queue", split_line=True)

    async def command_notif(self, msg: Message, *args):
        """Alias for `>>notification`"""
        await self.command_notification(msg, *args)

    async def command_notification(self, msg: Message, *args):
        """Change the notification setting. Or check the current setting.
        Options are:
            `all`: Get notified for any question.
            `urgent`: Get notified only for **urgent** questions.
            `never`: Don't get any notification."""
        if msg.author.id != self.question_taker.get(msg.channel.id):
            await self.say(msg.channel.id, (":no_entry: Notification Setting Failed",
                                            "You're not the question receptionist of this channel."))
            return

        if not args:
            if self.notification_setting[msg.channel.id] == ENotification.ALL:
                title = ":bell:"
            elif self.notification_setting[msg.channel.id] == ENotification.URGENT:
                title = ":bellhop:"
            else:
                title = ":no_bell:"
            title += " Current Channel's Notification Setting"
            await self.say(msg.channel.id, (title,
                                            "Current channel's setting: "
                                            "`" + str(self.notification_setting[msg.channel.id]) + '`'))
            return

        if args[0] == 'all':
            self.notification_setting[msg.channel.id] = ENotification.ALL
            await self.say(msg.channel.id, (":bell: Notification Setting of This Channel had been Changed",
                                            "From now on, you'll get notified for all questions."))
            log(msg.guild, msg.channel, "Changed [user]'s notification setting to: ALL", user=msg.author)
        elif args[0] == 'urgent':
            self.notification_setting[msg.channel.id] = ENotification.URGENT
            await self.say(msg.channel.id, (":bellhop: Notification Setting of This Channel had been Changed",
                                            "From now on, you'll get notified only for urgent questions."))
            log(msg.guild, msg.channel, "Changed [user]'s notification setting to: URGENT", user=msg.author)
        elif args[0] == 'never':
            self.notification_setting[msg.channel.id] = ENotification.NEVER
            await self.say(msg.channel.id, (":no_bell: Notification Setting of This Channel had been Changed",
                                            "From now on, you'll never get notified."))
            log(msg.guild, msg.channel, "Changed [user]'s notification setting to: NEVER", user=msg.author)
        else:
            await self.say(msg.channel.id, (":no_entry: Notification Setting Failed",
                                            "Unavailable argument."
                                            "Type `>>help notification` to see all available arguments."))

    async def command_answer(self, msg: Message, *args):
        """Start to answer the dequeued question."""
        if msg.author.id != self.question_taker.get(msg.channel.id):
            await self.say(msg.channel.id, (":no_entry: Answering Failed",
                                            "You're not the question receptionist of this channel."))
            return

        if not self.question_being_answered.get(msg.channel.id):
            await self.say(msg.channel.id,
                           (":no_entry: Answering Failed", "You haven't dequeued any question yet."))
            return

        if self.is_answering.get(msg.channel.id):
            await self.say(msg.channel.id, ("no_entry_sign: Answering Failed", "You're already answering a question."))
            return

        question = self.question_being_answered[msg.channel.id]
        msg_to_send = "Answering " + mention_user(question.questioner) + "'s"
        if question.is_private:
            msg_to_send += " **private**"
        if question.is_urgent:
            msg_to_send += " **urgent**"
        msg_to_send += " Question."
        msg_to_send += "\nType `>>end` to finish putting answer."
        msg_to_send += "\nType `>>cancel` to cancel answering question."
        msg_to_send += "\n__Other users are **safe** to talk.__"
        if question.is_private:
            msg_to_send += "\n**Your message will be instantly deleted after being stored.**"
        await self.say(msg.channel.id, (":ear: Receiving an Answer", msg_to_send))

        self.is_answering[msg.channel.id] = True
        self.answer_being_written[msg.channel.id] = ""

        log(msg.guild, msg.channel, "[user] started to write an answer to the following question:\n",
            self.question_being_answered[msg.channel.id], user=msg.author)

    async def command_ignore(self, msg: Message, *args):
        """Ignore the dequeued question.
        You can give the detail why the question is being ignored. Write sentences after `>>ignore`.
        You DON'T need to surround the sentences with quotes."""
        if msg.author.id != self.question_taker.get(msg.channel.id):
            await self.say(msg.channel.id, (":no_entry: Ignoring Failed",
                                            "You're not the question receptionist of this channel."))
            return

        if not self.question_being_answered.get(msg.channel.id):
            await self.say(msg.channel.id,
                           (":no_entry: Ignoring Failed", "You haven't dequeued any question yet."))
            return

        question = self.question_being_answered[msg.channel.id]

        await self.say(msg.channel.id, (":ballot_box_with_check: Ignored a Question",
                                        "Ignored " + mention_user(question.questioner) + "'s question"))

        msg_to_send1_title = ":frowning2: **Your question had been ignored.**\n"
        msg_to_send1_content = "There was no detail." if not args else "**Here's the reason:**\n" + " ".join(args)
        msg_to_send2_title = "The original question was:"
        if question.is_private:
            await self.send_dm(msg, (msg_to_send1_title, msg_to_send1_content), (msg_to_send2_title, question.content),
                               user=question.questioner, split_line=True)
        else:
            await msg.channel.send(mention_user(question.questioner))
            await self.say(msg.channel.id, (msg_to_send1_title, msg_to_send1_content),
                           (msg_to_send2_title, question.content),
                           split_line=True)

        log(msg.guild, msg.channel, "[user] ignored following question:\n",
            self.question_being_answered[msg.channel.id], "\n\nWith this reason:\n", " ".join(args),
            user=msg.author)

        del self.question_being_answered[msg.channel.id]

    async def command_rule(self, msg: Message, *args):
        """View the questioning rule the question receptionist set. Or set the rules.
        Provide additional argument `-set` to set the rules."""
        if self.is_writing_rule.get(msg.channel.id):
            await self.say(msg.channel.id, ("no_entry_sign: Setting Rule Failed", "You're already setting rules."))
            return

        if msg.author.id == self.question_taker.get(msg.channel.id):
            if args and args[0] == "-set":
                self.is_writing_rule[msg.channel.id] = True
                self.rule_being_written[msg.channel.id] = ""

                msg_to_send = "Setting this channel's questioning rules."
                msg_to_send += "\nType `>>end` to finish putting rules."
                msg_to_send += "\nType `>>cancel` to cancel setting rules."
                msg_to_send += "\n__Other users are **safe** to talk.__"
                await self.say(msg.channel.id, (":ear: Receiving Rules", msg_to_send))

                log(msg.guild, msg.channel, "[user] started to writing the rule of this channel.", user=msg.author)
            elif not self.question_rule.get(msg.channel.id):
                await self.say(msg.channel.id, (":x: No Rules", "There are no rules yet."))
            else:
                await self.say(msg.channel.id, (":point_up: Questioning Rules", self.question_rule[msg.channel.id]))
        else:
            if not self.question_rule.get(msg.channel.id):
                await self.say(msg.channel.id, (":x: No Rules", "There are no rules yet."))
            else:
                await self.say(msg.channel.id, (":point_up: Questioning Rules", self.question_rule[msg.channel.id]))

    @staticmethod
    def get_embed(fields, title, description, color, inline, split_line):
        to_embed = discord.Embed(title=title, description=description, color=color)
        files = []

        i = 1
        file_count = 1
        for field in fields:
            name = field[0] if field[0] else '\u200b'
            value = field[1] if field[1] else '\u200b'
            if split_line and len(fields) > i:
                value += '\n\u200b'
            for match in re.findall(FILE_PATTERN, value):
                value = value.replace(match, "[PICTURE_" + str(file_count) + "]")
                with open("data/attachments/" + match[6:] + ".png", 'rb') as file_to_attach:
                    files.append(discord.File(file_to_attach, filename="FILE_" + str(file_count) + ".png"))
                file_count += 1
            to_embed.add_field(name=name, value=value, inline=inline)
            i += 1

        return to_embed, files

    async def send_dm(self, msg: Message, *fields: Tuple[str, str], user: int=None, title="", description="",
                      color: int = 0x00fcff, inline=False, split_line=False):
        receiver = self.get_user(user) if user else msg.author
        
        to_embed = self.get_embed(fields, title, description, color, inline, split_line)
        try:
            await receiver.send(embed=to_embed[0])
            log(msg.guild, msg.channel, "Sent DM to [user] the following:\n", to_embed[0], user=receiver)
            if to_embed[1]:
                await receiver.send(files=to_embed[1])
        except discord.errors.Forbidden:
            log(msg.guild, msg.channel, "Failed to send DM to [user] the following:\n", to_embed[0], user=msg.author)
        
            await msg.channel.send(mention_user(receiver.id))
            msg_to_send = "\nI couldn't send DM to you."
            msg_to_send += "\nThere's a strong chance that you disallowed direct messages from server members."
            msg_to_send += "\nIt's not possible to send you the message even if you change the setting now."
            msg_to_send += "\n\nHere's the message I tried to send you:\n"
            await self.say(msg.channel.id, (":no_entry: Failed to Notify", msg_to_send))
            await msg.channel.send(embed=to_embed[0])

    async def say(self, channel: int, *fields: Tuple[str, str], title="", description="",
                  color: int = 0x00fcff, inline=False, split_line=False):
        if not self.get_channel(channel):
            self.remove_channel(channel)
            return
            
        to_embed = self.get_embed(fields, title, description, color, inline, split_line)
        channel = self.get_channel(channel)
        await channel.send(embed=to_embed[0])
        if to_embed[1]:
            await channel.send(files=to_embed[1])
    
    def add_channel(self, channel: int, user: int):
            self.question_taker[channel] = user
            self.notification_setting[channel] = ENotification.ALL
            self.is_answering[channel] = False
            self.is_writing_rule[channel] = False
            self.question_rule[channel] = ""

    def remove_channel(self, channel: int):
        try:
            for user in self.enqueueing_user[channel]:
                try:
                    del self.question_being_written[user]
                except KeyError:
                    pass
        except KeyError:
            pass
        for prop in self.attrs_to_save:
            try:
                del getattr(self, prop)[channel]
            except KeyError:
                pass

    def get_data(self):
        return (getattr(self, name) for name in self.attrs_to_save)

    def load(self):
        for attr in self.attrs_to_save:
            try:
                file = glob("data/raw/" + attr + "/*.txt")[-1]
                with open(file, 'rb') as attr_file:
                    super().__setattr__(attr, pickle.load(attr_file))
                log_bot("Loaded", attr, "of", file[:-4])
            except IndexError:
                log_bot("There was no", attr, "to load.")


if __name__ == '__main__':
    queue_bot = QueueBot()
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(queue_bot.start("NTc3NzU0NTQyODYzODEwNTg2.XNpqAA.u4kqSMbLKrOqCOccBjJZc1SRkEs"))
    except KeyboardInterrupt:
        loop.run_until_complete(queue_bot.logout())
    finally:
        loop.close()
